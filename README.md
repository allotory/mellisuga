# Mellisuga Q&A

## What is Mellisuga Q&A?

Mellisuga Q&A system is a open source Q&A platform for JSP/MySQL.

## Features

* Asking and answering questions.
* Voting, comments, best answer selection, follow-on and * closed questions.
* Complete user management including points-based reputation management.
* Create experts, editors, moderators and admins.
* Fast integrated search engine, plus checking for similar questions when asking.
* Categories and/or tagging.
* Easy styling with CSS themes.
* RSS, email notifications and personal news feeds.
* User avatars and custom fields.
* Private messages.
* Block users, IP addresses, and censor words.
* Integrate WYSIWYG or other text editors with editor/viewer modules.

## Installation for Development

If you want to test Eutoxeres IM as a standalone app for development, you will need
to install [`Eclipse`](http://www.eclipse.org/) as well.

To import from existent project.

## Contributing

Pull requests are being accepted! If you would like to contribute, simply fork
the project and make your changes.

##Support:

Support now is given by me.

## License

The GPL License.
